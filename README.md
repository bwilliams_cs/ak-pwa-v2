## npm

You’ll start by installing/running/building the PWA project.
Please follow the steps to run the react PWA of the project.

1. "yarn install" - to install necessary node modules dependency
2. "yarn start" - will run a debug mode of the application  with .env.development configuration
3. "yarn build:dev" - to create development build with .env.development configuration
3. "yarn build:rc" - to create rc build with .env.rc configuration
3. "yarn build:sandbox" - to create sandbox build with .env.sandbox configuration
3. "yarn build:production" - to create production build with .env.production configuration


