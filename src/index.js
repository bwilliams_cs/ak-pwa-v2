import React, { useEffect } from 'react';
import firebase from 'firebase'
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Login from './pages/login';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import {
  privateRouteMap,
  publicRouteMap
} from './routes';
import PrivateRoute from './routes/PrivateRoute';
import PublicRoute from './routes/PublicRoute';
import {
  BrowserRouter as Router,
  Switch
} from 'react-router-dom';
import _ from 'lodash';
import store from '../src/store';
import { Provider } from 'react-redux';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



const path = require('path');

axios.defaults.baseURL=process.env.REACT_APP_BASE_URL;

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: '#2D323E'
      }
    },
    status: {
      danger: 'orange',
    },
  });

  const routing = (
    // <StylesProvider injectFirst>
    <ThemeProvider theme={ theme }>
              <Provider store={ store }>
                <Router>
                <ToastContainer autoClose={ 5000 } />
                        <Switch>
                            {

                                _.map(publicRouteMap, route => <PublicRoute key={ 'publicRouter' } exact path={ route.path } component={ route.component } access={route.access}/>)
                            }
                            {
                                _.map(privateRouteMap, route => <PrivateRoute key={ 'privateRouter' } exact path={ route.path } component={ route.component } access={route.access}/>)
                            }
                        </Switch>
                </Router>
              </Provider>
    </ThemeProvider>
    // </StylesProvider>
);


// if ("serviceWorker" in navigator) {
//   navigator.serviceWorker
//     .register("./firebase-messaging-sw.js")
//     .then(function(registration) {
//       console.log("Registration successful, scope is:", registration.scope);
//     })
//     .catch(function(err) {
//       console.log("Service worker registration failed, error:", err);
//     });
// }

//navigator.serviceWorker.register('/firebase-messaging-sw.js')

serviceWorker.register(); 

// const firebaseConfig = {
//   apiKey: "AIzaSyBowimviPK8r-IGn6s2_YSkfrioq4InRCw",
//   authDomain: "pwa-v2-efdcd.firebaseapp.com",
//   databaseURL: "https://pwa-v2-efdcd.firebaseio.com",
//   projectId: "pwa-v2-efdcd",
//   storageBucket: "pwa-v2-efdcd.appspot.com",
//   messagingSenderId: "9671179593",
//   appId: "1:9671179593:web:bd791176156fc3d9ad2e98",
//   measurementId: "G-PG9LG4DGXT"
// };
// firebase.initializeApp(firebaseConfig);
// firebase.analytics();

// const messaging = firebase.messaging();
// messaging.requestPermission()
//   .then(function() {
//       console.log('Have permission');
//       return messaging.getToken();
//   })
//   .then(function(token) {
//     console.log(token);
//   })
//   .catch(function(err) {
//       console.log('Error Occured.');
//   })

//   messaging.onMessage(function(payload) {
//     console.log('onMessage: ', payload);
//   })

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("./firebase-messaging-sw.js")
    .then(function(registration) {
      console.log("Registration successful, scope is:", registration.scope);
    })
    .catch(function(err) {
      console.log("Service worker registration failed, error:", err);
    });
}



ReactDOM.render(
  routing,
//<Timer startTimeInSeconds="300" />,
document.getElementById('root')
);



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA


