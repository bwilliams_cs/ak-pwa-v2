import {
    combineReducers,
    createStore
} from 'redux';

import { routerReducer } from 'react-router-redux';
import userReducer from './userReducer';

const appReducer = combineReducers({
    routing: routerReducer,
    user: userReducer
});

const rootReducer = (state, action) => {
    if (action.type === 'USER_REMOVE') {
        state = undefined;
    }
    return appReducer(state, action);
};

export default createStore(rootReducer);
