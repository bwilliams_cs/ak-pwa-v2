const initialState = {
    user: null
};
const userReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case 'SET_USER':
        {
            return {   
                ...action.payload 
            };
        }
        default:
        {
            return state;
        }
    }
};

export default userReducer;
