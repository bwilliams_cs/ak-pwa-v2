import React from 'react';
import LoginPage from '../pages/login';
import HomePage from '../pages/home';
import MeetsPage from '../pages/meets';
import GruppPage from '../pages/group';
import ProfilePage from '../pages/profile';
import ChatPage from '../pages/chat';
import NavbarPage from '../pages/navbar';


const publicRouteMap = {
    Login: {
        path: '/login',
        component: LoginPage
    }

    
    
};


const privateRouteMap = {
    Meets: {
        //temp public
        path: '/meets',
        component: MeetsPage
    },
    Profile: {
        path: '/profil',
        component: ProfilePage
    },
    Chat: {
        path: '/chat',
        component: ChatPage
    },
    Grupp: {
        //temp public
        path: '/grupper',
        component: GruppPage
    },
    /* Navbar: {
        path: '*',
        component: NavbarPage
    },*/
    Home: {
        path: '*',
        component: '/profil'
        //access: [ localStorage.login_status ]
    }
};

export {
    publicRouteMap,
    privateRouteMap
};
