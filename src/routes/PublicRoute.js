import {
    Redirect,
    Route
} from 'react-router-dom';



import React from 'react';
import {useSelector} from 'react-redux';
import Cookies from 'universal-cookie';
const cookies = new Cookies();
const PublicRoute = ({ component: Component, ...rest }) => {
    const role = useSelector(({user}) => user? user.role : null );
    return (
        <Route { ...rest } render={ props => (
            //localStorage.getItem('jwt_access_token')
            cookies.get('id') 
            ?
                <Redirect to="/login" />
                : <Component { ...props } />
        ) } />
    );
};

export default PublicRoute;
