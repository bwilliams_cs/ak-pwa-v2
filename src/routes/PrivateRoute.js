import {
    Redirect,
    Route
} from 'react-router-dom';

import Home from '../pages/home';
import Navbar from '../pages/navbar'
import React from 'react';
import _ from 'lodash';
import {useSelector} from 'react-redux';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

const PrivateRoute = ({ component: Component, loggedInUser, access, ...rest }) => {
    //const role = useSelector(({user}) => user? user.role : null );
    return (
        <Route { ...rest } render={ props => (
            cookies.get('id') ? (
                <div>
                <Navbar/>
                    <Component { ...props } />
                </div>
            )
                
                //<Redirect to="/" />
                //<Home><Component { ...props } /></Home>
                : <Redirect to="/login" />
        ) } />
    );
};

export default PrivateRoute;
