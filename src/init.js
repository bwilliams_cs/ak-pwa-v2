const firebaseConfig = {
    apiKey: "AIzaSyBowimviPK8r-IGn6s2_YSkfrioq4InRCw",
    authDomain: "pwa-v2-efdcd.firebaseapp.com",
    databaseURL: "https://pwa-v2-efdcd.firebaseio.com",
    projectId: "pwa-v2-efdcd",
    storageBucket: "pwa-v2-efdcd.appspot.com",
    messagingSenderId: "9671179593",
    appId: "1:9671179593:web:bd791176156fc3d9ad2e98",
    measurementId: "G-PG9LG4DGXT"
  };
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

  const messaging = firebase.messaging();
  messaging.requestPermission()
    .then(function() {
        console.log('Have permission');
    })
    .catch(function(err) {
        console.log('Error Occured.');
    })