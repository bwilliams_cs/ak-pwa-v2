
import * as firebase from "firebase/app";
import "firebase/messaging";
const initializedFirebaseApp = firebase.initializeApp({
// Project Settings => Add Firebase to your web app
apiKey: "AIzaSyBowimviPK8r-IGn6s2_YSkfrioq4InRCw",
authDomain: "pwa-v2-efdcd.firebaseapp.com",
databaseURL: "https://pwa-v2-efdcd.firebaseio.com",
projectId: "pwa-v2-efdcd",
storageBucket: "pwa-v2-efdcd.appspot.com",
messagingSenderId: "9671179593",
appId: "1:9671179593:web:bd791176156fc3d9ad2e98",
measurementId: "G-PG9LG4DGXT"

});
const messaging = initializedFirebaseApp.messaging();
messaging.usePublicVapidKey(
// Project Settings => Cloud Messaging => Web Push certificates
"BLp_G6a3zjffUbSjxSsnOe5pngeEEMBtRha_aHE_J8rk_zDhxKIz-5tImELAFgcIve2V-S-RAtWjUvGndt8ry44"
);
export { messaging };