import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import ReplayIcon from '@material-ui/icons/Replay';
import IconButton from '@material-ui/core/IconButton';
import AppBar from '@material-ui/core/AppBar';
import _ from 'lodash';
import axios from 'axios';
import endPoints from './endPoints.json';
import jwtService from '../../services/jwtService';
import React, {useEffect, useState} from 'react';
import {Table, TableBody, TableHead, TableCell, TableRow, Typography, Grid, Divider, Toolbar} from '@material-ui/core';
import {withRouter} from 'react-router-dom';
import * as API from './api';
import {useDispatch, useSelector} from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import LocalizedStrings from 'react-localization';
import Cookies from 'universal-cookie';
import { makeStyles } from '@material-ui/core/styles';
import { toast } from 'react-toastify';

const cookies = new Cookies();
const translation = require('../../translation.json');
const t = new LocalizedStrings(translation);
t.setLanguage(cookies.get("kachingLang") ? cookies.get("kachingLang") : "SE");


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    alignContent: 'center'
  },
}));

function DashboardTable(props)
{

    const classes = useStyles();

    const dispatch = useDispatch();
    const [embeded_url, setembeded_url] = useState('');
    const [isFetching, setIsFetching] = useState(true);
    const browser = navigator.userAgent.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
    const role = useSelector(({user}) => user? user.role : null );

	/*useEffect(() => {
		API.getReport((data)=> {
      setembeded_url(_.get(data, 'url'));
      // setembeded_url(test);
      setIsFetching(false);
    }, role);
  }, []);*/
  
  const logout = () => {
    jwtService.logout();
    dispatch({
      type   : 'USER_REMOVE'
    })
    props.history.push("/login");
  }

  const reload = () => {
    setIsFetching(true);
    API.getReport((data)=> {
      setembeded_url(_.get(data, 'url'));
      setIsFetching(false);
    }, role);
  }
  
    return (
      <div >
          <AppBar style={{ backgroundColor: '#b6dbe4', height: 65}} position="static">
          <Toolbar className={classes.root}>
          <Typography className={classes.title} style={{ color: '#000000'}} variant="h6">{t.Dashboard}</Typography>
          <IconButton
              style={{ position: 'absolute', top: 10 , right: 10 , color: '#000000'}}
              onClick={ () =>  logout() }
            >
              <ExitToAppIcon />
            </IconButton>
            <IconButton
              style={{ position: 'absolute', top: 10 , right: 60 , color: '#000000'}}
              onClick={ () =>  reload() }
            >
              <ReplayIcon />
            </IconButton>
          </Toolbar>
          </AppBar>
          { isFetching &&
                <CircularProgress color={'inherit'} disableShrink style={ { position: 'fixed', left: '50%', top: '50%', zIndex: 9999 } } />
          }
          <iframe style={{ position: 'absolute', top: 65, height: '100%', width: '100%' }} scrolling="no" src={embeded_url}></iframe>
    </div>)
}

export default withRouter(DashboardTable);

