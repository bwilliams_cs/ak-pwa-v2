import axios from 'axios';
import { toast } from 'react-toastify';
import React from 'react';
import LocalizedStrings from 'react-localization';
import Cookies from 'universal-cookie';
export const GET_REPORT = '[DASHBOARD] GET REPORT';
let DASHBOARD_URL = 'api/analytics';
let ORGANIZATION_URL = '/api/organization';

const cookies = new Cookies();
const translation = require('../../translation.json');
const t = new LocalizedStrings(translation);
t.setLanguage(cookies.get("kachingLang") ? cookies.get("kachingLang") : "EN");


export function getReport(success,role)
{
	const requestOrg = axios.get(ORGANIZATION_URL+'/');
		requestOrg.then((response) => {
	
		let orgId = response.data.response.id;
		const request_quick = role === 'superadmin' ? 
							axios.get(DASHBOARD_URL+'/dashboards/super/embedded-url') :
							axios.get(DASHBOARD_URL+'/dashboards/embedded-url?organizationId='+orgId)

		request_quick.then(function (response_quick) {
			const loader = document.getElementById("loader");
			if(loader)
			loader.style.display = "none";
			success(response_quick.data)
		}).catch(function (error) {
			success(null);

			if (error.response) {
				console.log(error.response);
				let trace_id = (error.response.data.traceId) ? error.response.data.traceId:'';
				let status_code = (error.response.status) ? error.response.status:'';
				let error_message = (error.response.data.errors) ? error.response.data.errors[0].errorMessage.en:'';
				if(error_message){
				toast.error(<div>{' Status code: '+status_code}<br />{' TraceId: '+trace_id}<br />{'Message : ' +error_message}</div>)
				}
			}else if (error.request) {
				console.log(error.request);
				toast.error(error.message)
			}else {
				console.log(error);
				toast.error(error.message)
			}
			
		});
		}).catch(function (error) {
			console.log(error);
		});
}
