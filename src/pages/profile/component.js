import React, { useState, useEffect } from 'react';
import { Button, TextField, makeStyles } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import axios from 'axios';
import _, { split, get } from 'lodash';
import Cookies from 'universal-cookie';

const cookies = new Cookies();


const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      '& > * + *': {
        marginLeft: theme.spacing(2),
      },
    },
  }));

function Component(props) {

    const [form, setForm] = useState(null);
    const [patch, setPatch] = useState(null);
    const [edit, setEdit] = useState(false);
    const [loading, setLoading] = useState(true);
    const classes = useStyles();

    
    
        
   function getProfile() {
        axios.post('http://localhost:1234/profil/'+cookies.get('id'))
        .then(res => {
            
            localStorage.setItem('profile_name', res.data.Name);
            localStorage.setItem('profile_record', res.data.RecordTypeId);
            localStorage.setItem('profile_email', res.data.Email);
            localStorage.setItem('group', res.data.Group);
            localStorage.setItem('profile_phone', res.data.Mobile);
            localStorage.setItem('profile_type', res.data.Type);
            localStorage.setItem('profile_address', res.data.Address);
            localStorage.setItem('profile_category', res.data.Category);
            setForm({...form , 
                Name: localStorage.profile_name, 
                Email: localStorage.profile_email,
                MobilePhone: localStorage.profile_phone,
                Typ__c: localStorage.profile_type,
                Gatuadress__c: localStorage.profile_address,
                RecordTypeId: localStorage.profile_record
                //Kategori__c: localStorage.profile_category
            });
            setLoading(false);

        //localStorage.setItem('profile_id',res.data.id);

            
        })
        .catch(error => console.log(error))
    }
    /*function getId() {

        setId({...id, Id: localStorage.profile_id})
    }*/

    function patchProfile() {
        const split = form.Name.split(' ')
        setPatch({...patch,
            FirstName: split[0],
            LastName: split[1],
            Email: form.Email,
            MobilePhone: form.MobilePhone,
            //RecordTypeId: form.RecordTypeId,
            Typ__c: form.Typ__c,
            Gatuadress__c: form.Gatuadress__c
            //Kategori__c: form.Kategori__c


        })
        setEdit(false)
            
        
        
        
    }

    useEffect(() => {
        if(patch){
        
        axios.post('http://localhost:1234/profil_patch/'+cookies.get('id'), patch)
            .then(res => {
            })
            .catch(error => console.log(error))
        }

    },[patch]);

    useEffect(() => {
        getProfile()
    },[]);

    /*useEffect(()=> {
        if(id){
        //getProfile()
        }
    },[id]);*/

    
    return(
<div>
    { loading? 
    (
        <div className={classes.root}>
        <CircularProgress color="secondary" />
      </div>
    ) 
    :
    (
    <div>
    <TextField 
    error = {_.get(form, 'Name', '').split(' ').length > 2 || _.get(form, 'Name', '') === ''}

    helperText = {_.get(form, 'Name', '').split(' ').length > 2, 'Max ett mellan slag'}
    
    disabled = {!edit}
    label = 'Namn' 
    variant = 'outlined'
    value = {_.get(form , 'Name', '')}
        onChange={event => setForm({...form,  Name: event.target.value})} /> 
    <TextField 
    disabled
    label = 'Mail' 
    variant = 'outlined'
    value = {_.get(form , 'Email', '')}
        onChange={event => setForm({...form,  Email: event.target.value})}/>  
       
    <TextField 
    disabled = {!edit}
    label = 'Mobil' 
    variant = 'outlined'
    value = {_.get(form , 'MobilePhone', '')}
        onChange={event => setForm({...form,  MobilePhone: event.target.value})}/>   
    
    <TextField 
    disabled = {!edit}
    label = 'Adress' 
    variant = 'outlined'
    value = {_.get(form , 'Gatuadress__c', '')}
        onChange={event => setForm({...form,  Gatuadress__c: event.target.value})}/>
    
    <TextField 
    disabled = {true}
    label = 'Typ' 
    variant = 'outlined'
    value = {_.get(form , 'Typ__c', '')}
        onChange={event => setForm({...form,  Typ__c: event.target.value})}/>
    
   
    <Button 
         onClick={_=> setEdit(!edit)}> { edit? "Cancel": "Edit" } </Button>
     {edit &&
         (<Button 
        
         onClick={patchProfile}>Save</Button>)}
    </div>
    )
    }
</div>
    
    )
}
export default Component;