import React, { useState, useEffect } from 'react'
import axios from 'axios';
import _, { split, get } from 'lodash';
import CircularProgress from '@material-ui/core/CircularProgress';
import Cookies from 'universal-cookie';
import { Box, Grid, TextField, makeStyles, Button } from '@material-ui/core';
const cookies = new Cookies();

const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      '& > * + *': {
        marginLeft: theme.spacing(2),
      },
    },
  }));

function Component(props) {





    const [view, setView] = useState(false);
    const [meets, setMeets] = useState(null);
    const [loading, setLoading] = useState(true);
    const [get, setGet] = useState(false);
    const [forceUpdate, setForceUpdate] = useState(0);
    const classes = useStyles();


    function getMeets() {
        axios.post('http://localhost:1234/meets/'+cookies.get('groupId'))
            .then(response => {
                console.log(response)
                setMeets({...meets, meets_array: response.data.records})
                
                /*setMeets({...meets, 
                    Date: response.data.Date,
                    Address: response.data.Address
                    
                })*/
                //setHost({...host, Id: response.data.Host})
                

            })
            .catch(error => {console.log(error)})

    }

    function getParticipants() {
        meets.meets_array.map(meet => {
            return axios.get('http://localhost:1234/participants/'+ meet.Id)
                    .then(response => {
                    meet.participants = response.data.records;

                    setLoading(false);
                    
                    return meet;
                    })
                    .catch(error => {console.log(error)})
        })

    }

    function flipBoolean(index) {
        //console.log(meets.meets_array[index].open)
        meets.meets_array[index].open = !meets.meets_array[index].open;
        
        setForceUpdate(forceUpdate + 1);
    }
    /*async function getHost(host) {
        await axios.post('http://localhost:1234/host/'+host)
            
            .then(response => {
                //console.log(response.data.Name)
                console.log(response.data.Name);
                return response.data.Name
                //setMeets({...meets, Host: response.data.Name})
                //setHost({...host, host_array: response})
            })
            .catch(error => console.log(error))
        
    }*/

    /*if(meets){
        var i;
        for(i = 0; i<meets.meets_array.length; i++){
            console.log(meets.meets_array[i].V_rd_f_r_fikat__c);
            console.log(getHost(meets.meets_array[i].V_rd_f_r_fikat__c));
        }

    }*/


    useEffect(() => {
        //setForm({...form, Grupp__c: localStorage.group});
        getMeets();
    },[]);

    /*useEffect(() => {
        getHost();
    },[host]);*/

    useEffect(() => {
        if(meets){
        meets.meets_array.map(meet => {
            meet.open = false;
        
        })
        setGet(true);
        }
    },[meets]);

    useEffect(() => {
        if(meets){
            getParticipants();
        }
    },[get]);

    /*useEffect(() => {
        if(meets){
            console.log(meets)
        }
    },[meets]);*/

/*if(meets){
    setHost({...host, Array: meets.meets_array.map(meet => {
    const name = getHost(meet.V_rd_f_r_fikat__c)

    const renderArray = 
        {Name: meet.Name, Date: meet.Datum__c, Host: name}

    console.log(renderArray)
    return renderArray;
})})
    
}*/


return(
    <div>
        { loading? (
        <div className={classes.root}>
        <CircularProgress color="secondary" />
        </div>
        )
        :
        (<div>
            {meets.meets_array.map((meet, index) => {
                //Loop through the array of objects
                

                    return <h6>
                        
                    <div>
                        Namn: { meet.Name } 
                        Datum: { new Date(meet.Datum__c).toUTCString() }  
                        Värd: { meet.V_rd_f_r_fikat__r.Name } 
                        Adress: { meet.Gatuadress_ort_portkod__c}                      
                        
                        <Button onClick={_=> flipBoolean(index)}

                        >
                        { meet.open ? '-' : '+'}
                         </Button>
                    </div>

                    { meet.open && (<div>
                        Deltagare: { meet.participants.map(ppl =>  
                                        <h6>{ppl.Namn__c}</h6> ) }
        
                    </div>)}
                </h6>
                
                
            })}
        </div>)}
    </div>

)
}
export default Component;
