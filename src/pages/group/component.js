import React, { useState, useEffect } from 'react'
import { Box, Select, MenuItem,InputLabel,Grid, FromControl, makeStyles, Button } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import axios from 'axios';
import _, { get } from 'lodash';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      '& > * + *': {
        marginLeft: theme.spacing(2),
      },
    },
  }));

function Edit(props){

    const [form, setForm] = useState(null);
    const [members, setMembers] = useState(null);
    const [loading, setLoading] = useState(true);
    const classes = useStyles();

    function getGroup() {
        axios.post('http://localhost:1234/grupper/'+cookies.get('groupId'))
            .then(res => {
                localStorage.setItem('group_name', res.data.Name)
                localStorage.setItem('group_leader', res.data.Leader)
                
            })
            .catch(error => console.log(error))

    }

    function getMembers() {
        axios.post('http://localhost:1234/members/'+cookies.get('groupId'))
            .then(res => {
                console.log(res.data.records);
                localStorage.setItem('member_array', res.data.records);
                setMembers({...members, memberList: res.data.records})
                setLoading(false);
            })
            .catch(error => console.log(error))
    }

    function setGroup() {
        setForm({...form, Grupp__c: localStorage.group})
    }

    useEffect(() => {
        //setGroup();
        getGroup();
        getMembers();
    },[]);

    /*useEffect(() => {
        if(form){
            getGroup();
            getMembers();
        }
    },[form]);*/
    /*function textFieldAdd(labelName,variable,form){
        //var labelName = labelName;
        return(
            
            <TextField label = {labelName} value = {_.get(form, variable, '')}
            onChange={event => setForm({...form, [variable]: event.target.value})}/>
        )

    }*/

     


    return(
        <div>
            
        { loading? 
        (
            <div className={classes.root}>
            <CircularProgress color="secondary" />
            </div>
        )
        :
        (
        <div>
            <div> { localStorage.group_name} </div>
            
            {members.memberList.map((members) => {
                //Loop through the array of objects
                if(members.Id === localStorage.group_leader){
                    return <h6>
                    <div>Grupp Ledare: { members.Name } Roll: {members.Typ__c} Mobil: { members.MobilePhone }
        
                </div>
                </h6>
                }
                else{
                return <h6>
                    <div>Namn: { members.Name } Roll: {members.Typ__c} Mobil: { members.MobilePhone }
        
                </div>
                </h6>
                }
            })}
        </div>
        )}
        </div>
    )

}

export default Edit;