import { Box, Grid, Typography } from '@material-ui/core';
import React, { useState } from 'react'

import Component from './component';
import customstyles from './styles.css';

const Login = () => {
    return (
        <Grid container style={customstyles} justify='space-evenly' className="login" >
            <Grid item xs={12} md={7} lg={9}>
                <Box className="welcome">
                    <Typography
                    variant="h2">
                    <div><span>Äldrekontakt</span></div>
                    </Typography>
                </Box>
            </Grid>
            <Grid item xs={12} md={5} lg={true} className="control">
                <Box display="flexBox" >
                    <div className="heading">
                        <h6>Logga in till ditt konto</h6>
                    </div>
                    <div>{<Component />}</div>
                </Box>
            </Grid>
        </Grid>
    )
}

export default Login;
