import {Button, Grid, TextField, Icon, InputAdornment, Typography} from '@material-ui/core';
import React, {useEffect, useRef, useState} from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import {useDispatch } from 'react-redux';
import Cookies from 'universal-cookie';

//import Formsy from 'formsy-react';
//import {TextFieldFormsy} from '../../shared/formsy';
import jwtService from '../../services/jwtService';
import {
    withRouter
} from 'react-router-dom';
import { toast } from 'react-toastify';
import axios from 'axios';
import _, { set } from 'lodash';

const cookies = new Cookies();

const useStyles = makeStyles(() =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        button: {
            backgroundColor: '#850168',
            color: '#fff',
            borderRadius: 0,
            padding: '9px 15px',
            width:'300px'
        }
    }),
);

function Component(props)

{
    const [form, setForm] = useState(null);

    function Login() {
        axios.post('http://localhost:1234/login', form)
            .then(res => {
                console.log(res);
                cookies.set('id', res.data.id);
                cookies.set('groupId', res.data.gruppId)
                localStorage.setItem('profile_id',res.data.id);

                    if(res.data.boolean){
                    localStorage.setItem('jwt_access_token',res.data.boolean);
                    //console.log(localStorage.login_status);
                
                    props.history.push("/profil");
                    }
            }).catch(error => console.log(error))
    }
    const classes = useStyles();
    //const [isFormValid, setIsFormValid] = useState(false);
    const formRef = useRef(null);
    const dispatch = useDispatch();


    function handleChange(){
        const loginButton = document.getElementById("loginButton");
        loginButton.style.pointerEvents = "false";
    }

    /*function handleSubmit({email, password})
    {
        jwtService.signInWithEmailAndPassword(email, password)
            .then((response) => {
                dispatch({
                    type   : 'SET_USER',
                    payload: { ...response }
                })
                props.history.push("/");
            })
            .catch(error => {
                toast.error(error.message)
            })
    }*/

    

    return (
            <Grid
                //onValidSubmit={handleSubmit}
                //onValid={() => {}}
                //onInvalid={() => {}}
                //ref={formRef}
            >
                <Grid
                    container
                    direction="column"
                    alignItems="center"
                    justify="center"
                    style={{ marginTop: 100 }}
                >

                <TextField
                    type="text"
                    name="email"
                    label="Användarnamn"
                    value={_.get(form, 'Email', '')}

                    validations={{
                        minLength: 4
                    }}
                    validationErrors={{
                        minLength: 'Min character length is 4'
                    }}
                    variant="outlined"
                    requiredSynthetic
                    //style={{ padding: 50 }}
                    onChange={event => setForm({...form, Email: event.target.value})}
                />


                <TextField
                    type="text"
                    name="password"
                    label="Lösenord"
                    value={_.get(form, 'Ak_Password__c', '')}
                    validations={{
                        minLength: 4
                    }}
                    validationErrors={{
                        minLength: 'Min character length is 4'
                    }}
                    variant="outlined"
                    required
                    onChange={event => setForm({...form, Ak_Password__c: event.target.value})}
                />

                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    aria-label="LOG IN"
                    id="loginButton"
                    //disabled={!isFormValid}
                    value="legacy"
                    className={classes.button}
                    onClick={Login}
                >
                    Logga in
                </Button>
                </Grid>
            </Grid>
    );
}

export default withRouter(Component);