import React, { useState, useEffect } from 'react'
import { Box, Select, makeStyles, MenuItem,InputLabel,Grid, FromControl, TextField, Button } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import axios from 'axios';
import _, { get } from 'lodash';
import Cookies from 'universal-cookie';
import io from "socket.io-client";
import customstyles from './styles.css';
import { messaging } from "../../init-fcm";



//const socket = io.connect("http://localhost:1234");
const cookies = new Cookies();


const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      '& > * + *': {
        marginLeft: theme.spacing(2),
      },
    },
  }));

function Chat(props){

    //const [form, setForm] = useState(null);
    const [messages, setMessages] = useState(null);
    const [send, setSend] = useState(null);
    const classes = useStyles();

    // socket.on("update", data => {
    //     const message = JSON.parse(data);
    //     // setMessages(messages.push(message));
    //     setMessages([...messages, message])
    //   })

    function getMessages() {
        axios.get('http://localhost:1234/chat/'+cookies.get('groupId'))
            .then(res => {
                //localStorage.setItem('member_array', res.data.records);
                setMessages({...messages, messageList: res.data.records})
            })
            .catch(error => console.log(error))
    }

    function sendMessage() {
        axios.post('http://localhost:1234/send', send)
            .then(res => {
                console.log(res.data);
                setSend({...send, Message__c: ''})
                //const socket = io.connect("http://localhost:1234");

            })
            .catch(error => console.log(error))
    }


    useEffect(() => {
        getMessages();
    },[]);

    useEffect(() => {
        messaging.requestPermission()
          .then(async function() {
      const token = await messaging.getToken();
      console.log(token);
          })
          .catch(function(err) {
            console.log("Unable to get permission to notify.", err);
          });
        navigator.serviceWorker.addEventListener("message", (message) => console.log(message));
      }, []);

    /*useEffect(() => {
        if(form){
            getGroup();
            getMembers();
        }
    },[form]);*/
    /*function textFieldAdd(labelName,variable,form){
        //var labelName = labelName;
        return(
            
            <TextField label = {labelName} value = {_.get(form, variable, '')}
            onChange={event => setForm({...form, [variable]: event.target.value})}/>
        )

    }*/

     


    return(
        <div>
            
        { !messages?
        (
            <div className={classes.root}>
            <CircularProgress color="secondary" />
            </div>
        )
        :
        (<div>
            
            {messages.messageList.map((message) => {
                //Loop through the array of objects
                if(message.Sender__c === cookies.get('id')){
                    return <div>
                    <div>You: { message.Message__c }
        
                    </div>
                    <h6> { new Date(message.CreatedDate).toUTCString() } </h6>
                    </div>
                }
                else{
                return <div>
                    <div> { message.Sender__r.Name }: { message.Message__c }
        
                    </div>
                    <h6> { new Date(message.CreatedDate).toUTCString() } </h6>
                </div>
                }
            })}
        </div>)}
        
        <div id="send">
        <TextField 
        id="textField"
        variant = 'outlined'
        value = {_.get(send , 'Message__c', '')}
        onChange={event => setSend({...send,  Message__c: event.target.value, 
                                              Grupp__c: cookies.get('groupId'), 
                                              Sender__c: cookies.get('id')})}
        />
        
        <Button
            disabled = {_.get(send, 'Message__c', '') === ''}
            type="submit"
            id="button"
            variant="contained"
            color="primary"
            aria-label="Send"
            onClick={sendMessage}
        >
            Send
        </Button>
        </div>
        </div>
    )

}

export default Chat;