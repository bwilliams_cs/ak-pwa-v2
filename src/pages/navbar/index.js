import React, { useState } from 'react';
import { NavLink } from "react-router-dom";
import { Button, withStyles, Menu, MenuItem, ListItemIcon, ListItemText } from '@material-ui/core';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import Component from './components'
import customstyles from './styles.css';


const StyledMenu = withStyles({
    paper: {
      //border: '1px solid #d3d4d5',
    },
  })((props) => (
    <Menu
      elevation={0}
      getContentAnchorEl={null}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      {...props}
    />
  ));
  
  const StyledMenuItem = withStyles((theme) => ({
    root: {
      '&:focus': {
        backgroundColor: theme.palette.primary.main,
        '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
          color: theme.palette.common.white,
        },
      },
    },
  }))(MenuItem);
  
  export default function CustomizedMenus() {
    const [anchorEl, setAnchorEl] = React.useState(null);
  
    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
  
    const handleClose = () => {
      setAnchorEl(null);
    };
  
    return (
      <div /*container styles = {customstyles} className="menu"*/>
        <Button
          /*className= "main"
          aria-controls="customized-menu"
          aria-haspopup="true"
          variant="contained"
          color="primary"*/
          onClick={handleClick}
        >
          Open Menu
        </Button>
        <StyledMenu
          //className=""
          //id="styleMenuItem"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <StyledMenuItem
            //className=""
            id="styleMenuItem"
          >
            <NavLink 

             id = "menuLink"
             //activeClassName="is-active" 
             to="/meets">
              Träffar
            </NavLink>
            
          </StyledMenuItem>

          <StyledMenuItem>
            <NavLink
              className="navbar-item"
              activeClassName="is-active"
              to="/grupper"
            >
              <ListItemText primary="Grupp" />
            </NavLink>
            
          </StyledMenuItem>

          <StyledMenuItem>
          <NavLink
              className="navbar-item"
              activeClassName="is-active"
              to="/profil"
            >
              <ListItemText primary="Profil" />
            </NavLink>
            
          </StyledMenuItem>

          <StyledMenuItem>
            <NavLink 
             className="navbar-item" 
             activeClassName="is-active" 
             to="/chat">
              <ListItemText primary="Chat" />
            </NavLink>
            
          </StyledMenuItem>

          <StyledMenuItem>
          <Component>
              <ListItemText primary="Logout" />
            </Component>
            
          </StyledMenuItem>
        </StyledMenu>
      </div>
    );
  }
/*const Navbar = () => {
    const [isOpen, setOpen] = useState(false);
    return(
        
    <div>
        <nav
      className="navbar is-primary"
      role="navigation"
      aria-label="main navigation"
    >
      <div className="container">
        <div className="navbar-brand">
          <a
            role="button"
            className={`navbar-burger burger ${isOpen && "is-active"}`}
            aria-label="menu"
            aria-expanded="false"
            onClick={() => setOpen(!isOpen)}
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div>

        <div className={`navbar-menu ${isOpen && "is-active"}`}>
          <div className="navbar-start">
            <NavLink 
             className="navbar-item" 
             activeClassName="is-active" 
             to="/meets">
              Träffar
            </NavLink>

            <NavLink
              className="navbar-item"
              activeClassName="is-active"
              to="/grupper"
            >
              Grupper
            </NavLink>

            <NavLink
              className="navbar-item"
              activeClassName="is-active"
              to="/profil"
            >
              Profil
            </NavLink>
             <Component/>
            
          </div>
          <div className="navbar-end">
            <div className="navbar-item">
              
            </div>
          </div>
        </div>
      </div>
    </nav>
   
</div>
    )


}

export default Navbar;*/