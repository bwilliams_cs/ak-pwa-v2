import React, { useState, useEffect } from 'react'
import axios from 'axios';
import _, { split, get } from 'lodash';
import Cookies from 'universal-cookie';
import { Button } from '@material-ui/core';
import {
    withRouter,
    Redirect,
    Route
} from 'react-router-dom';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const cookies = new Cookies();


const useStyles = makeStyles(() =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        button: {
            backgroundColor: '#850168',
            color: '#fff',
            borderRadius: 0,
            padding: '9px 15px',
            width: '100%',
            

        }
    }),
);


function Component(props) {

    const classes = useStyles();
    
    

    function logout(){

        cookies.remove('id');
        cookies.remove('groupId');
        props.history.push('/login');
    }
        return(
            
            
            <Button
            //type="submit"
            //variant="contained"
            //color="primary"
            //aria-label="LOG OUT"
            id="logoutButton"
            //disabled={!isFormValid}
            //value="legacy"
            className={classes.button}
            onClick={logout}>
                Logout
            </Button>
                
        )

}

export default withRouter(Component);