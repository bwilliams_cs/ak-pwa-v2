

const ACCESS_TOKEN_KEY = 'auth.access_token';

const storage = {
    get: (key) => {
        return window.localStorage.getItem(key);
    },
    set: (key, val) => {
        return window.localStorage.setItem(key, val);
    },
    remove: (key) => {
        return window.localStorage.removeItem(key);
    }
};

const setAuthToken = (token) => {
    storage.set(ACCESS_TOKEN_KEY, token);
};

const getAuthToken = () => {
    return storage.get(ACCESS_TOKEN_KEY);
};

const removeAuthToken = () => {
    storage.remove(ACCESS_TOKEN_KEY);
};

const clearStore = () => {
    window.localStorage.clear();
};

export {
    storage,
    clearStore,
    getAuthToken,
    setAuthToken,
    removeAuthToken
};
