import { Link, Redirect, withRouter } from "react-router-dom";

import Cookies from 'universal-cookie';
import React from 'react';
import _ from 'lodash';
import axios from  'axios';
import { config } from '../utils/ApiConfig';
import history from '../../@history';
import jwtDecode from 'jwt-decode';
import LocalizedStrings from 'react-localization';
import { toast } from 'react-toastify';

let CATALOG_URL = config.CATALOG_URL;
let ORGANIZATION_URL = config.ORGANIZATION_URL;
let DASHBOARD_URL = config.DASHBOARD_URL;
let SCOPES = config.SCOPES;
let IDLE_TIME_EXP = config.IDLE_TIME_EXP;
const cookies = new Cookies();

let refreshTokenRequest;

class jwtService {
    constructor()
    {
		this.setInterceptors();
	    this.handleAuthentication();
	}

	/* Avoiding multiple refresh token  in parallel */
	getRefreshTokenRequest  = () => {
		if (!refreshTokenRequest) {
		  refreshTokenRequest = this.refreshToken();
		  refreshTokenRequest.then(this.resetAuthTokenRequest, this.resetAuthTokenRequest);
		}
		return refreshTokenRequest;
	  }
	  
	resetAuthTokenRequest = () => {
		refreshTokenRequest = null;
	}

	/* end */
	
    setInterceptors = () => {
		// Response interceptors
        this.axiosInterceptor = axios.interceptors.response.use(response => {
			return response;
        }, err => {
			if ( _.get(err.response, 'status') === 401 &&
				!err.config.__isRetryRequest && 
				_.get(err.config, 'url').replace(err.config.baseURL, "") !== '/token') // no '/token'  request falls below
			{
				axios.interceptors.response.eject(this.axiosInterceptor); // ejecting the interceptor to avoid looping calls.
				return this.getRefreshTokenRequest().then(_ => {
					this.setInterceptors();
					err.config.headers['Authorization'] = 'Bearer '+this.getAccessToken();
					return axios.request(err.config);
				  }).catch((error) => {
					this.setSession(null);
					window.location.reload();
					this.setInterceptors();
					const tokenError = new Error('User authentication failed');
					return Promise.reject(tokenError);
				  }
				  );
			}
			return  Promise.reject(err);
        });
	};

	handleRefreshToken = () => {
		let access_token = this.getAccessToken();

        if ( !access_token )
        {
			return;
        }
        if ( this.isAuthTokenValid(access_token) )
        {
            this.setSession(access_token);
        }
        else
        {
            let path = window.location.pathname;
            if(path.includes('/signup')!= true){
            	this.setSession(null);
            }else{
            	this.setSession(null);
            }
        }
    };

	handleAuthentication = () => {
		let access_token = this.getAccessToken();
		if(!access_token) {
		  return;
		} else if(this.isAuthTokenValid(access_token)) {
		  this.setSession(access_token);
		} else {
		  // This method handles token expiration, when user reloads the browser
		  this.handleRefreshToken();
		}
	};

	hasUserIdleTimeExceeded = () => {
		const lastAccessed = this.getLastAccessedTime();
		const currentTime = Date.now() / 1000;
		let seconds = currentTime - lastAccessed;
		return (seconds >= IDLE_TIME_EXP);
	};

    //~ createUser = (data) => {
        //~ return new Promise((resolve, reject) => {
            //~ axios.post('/api/auth/register', data)
                //~ .then(response => {
                    //~ if ( response.data.user )
                    //~ {
                        //~ this.setSession(response.data.access_token);
                        //~ resolve(response.data.user);
                    //~ }
                    //~ else
                    //~ {
                        //~ reject(response.data.error);
                    //~ }
                //~ });
        //~ });
	//~ };
	
	unAuthorisedAccess = () => {
		toast.error('You are not authorized to access this page');
		this.setSession(null);
		history.push("/login");
	}

    signInWithEmailAndPassword = (email, password) => {
		this.setSession(null);
		let access_token = this.getAccessToken();
		if(access_token){
			const aheaders_old = this.getEncodedString();
			const data_old = "token="+access_token;
			axios.post('/revoke', data_old, {
	                headers: aheaders_old
	            }).then(response => {
					console.log(response.status);
				}).catch(function (error) {
					if (error.response) {
						console.log(error.response);
					}else if (error.request) {
						console.log(error.request);
					}else {
						console.log(error);
					}
			  });
		}

		const aheaders = this.getEncodedString();
		const cheaders = {
		  'Content-Type': 'application/x-www-form-urlencoded'
		}

		const data = "grant_type=password&username="+email+"&password="+encodeURIComponent(password);
        return new Promise((resolve, reject) => {
            axios.post('/oauth2/token', data, {
                headers: aheaders
            }).then(response => {
                if ( response.data.access_token)
                {
                    this.setSession(response.data.access_token);
                    localStorage.setItem('jwt_id_token', response.data.id_token);
                    localStorage.setItem('jwt_refresh_token', response.data.refresh_token);
                    let decodedJwt = jwtDecode(response.data.id_token);
                    let user = this.setUserData(decodedJwt);
					//let user = this.setUserRole(decodedJwt.sub);
                    resolve(user);
                    let path = window.location.pathname;
                    if(user.role){
                    	if(user.role == 'superadmin' && path.includes('/superadmin') != true){
							history.push("/");
						}else if(user.role == 'partneradmin' && path.includes('/partneradmin') != true){
							this.unAuthorisedAccess();
						}else if(user.role!=='onboard'){
								history.push("/");
						}
                    }else{
                    	this.setUserData(decodedJwt)
							.then( response => {
								user=response;
								resolve(user);
								//let user = this.setUserRole(decodedJwt.sub);
			                    let path = window.location.pathname;
			                    if(user.role == 'superadmin' && path.includes('/superadmin') != true){
									history.push("/");
								}else if(user.role == 'partneradmin' && path.includes('/partneradmin') != true){
									this.unAuthorisedAccess();
								}else if(user.role!=='onboard'){
									history.push("/");
								}
						});
                    }
                }
                else
                {
                    reject(response.data.error_description);
                }
            }).catch(function (error) {
				let message = {};
				if (error.response) {
					console.log(error.response);
					message['traceId'] = (error.response.data.traceId) ? error.response.data.traceId:'';
					message['status'] = (error.response.status) ? error.response.status:'';
					message['message'] = (error.response.data.errors) ? error.response.data.errors[0].errorMessage.en:'';
					if(message['status']===401){
						message['message']="Invalid Username or Password";
					}
					if(message['message']){
						console.log(message);
						reject(message);
					}else if(error.response.data.error_description){
						message['message'] = error.response.data.error_description;
						reject(message);
					}else{
						message['message'] = "Login Fail";
						reject(message);
					}
				}else if (error.request) {
					console.log(error.request);
					message['message'] = error.message;
					reject(message);
				}else {
					console.log(error);
					message['message'] = error.message;
					reject(message);
				}
				//reject('Login Fail');
		  });
        });
	};

    signInWithToken = () => {
		let organizationid = this.getOrganizationId();
		const headers = {
		  'Authorization': 'Bearer '+this.getAccessToken()
		}
		delete axios.defaults.headers.common['organizationId'];
        return new Promise((resolve, reject) => {
            axios.get('/userinfo', {
                headers: headers
            })
                .then(response => {
                    if ( response.data.sub )
                    {
						this.setSession(this.getAccessToken());
						let user = this.setUserData(response.data);
						//let user = this.setUserRole(response.data.sub);
						let path = window.location.pathname;
						if(organizationid){
							const request = axios.get(ORGANIZATION_URL);
					        request.then((responseData) => {
								let data_to_post_qs = {"organizationId":responseData.data.response.id,"email":responseData.data.response.contactEmail}
								const qs_request = axios.post(DASHBOARD_URL+'/users/register', data_to_post_qs);

								user = this.checkForOnboard(user);
								user.then( userResponse => {
									userResponse.data.logoURL = (responseData.data.response.logoUrl) ? responseData.data.response.logoUrl:'';
									user=userResponse;
									resolve(user);
								});
								resolve(user);
								user = this.switchAdminaAndSuper(user,path);
							}).catch(function (error) {
								if (error.response) {
									console.log(error.response);
									let trace_id = (error.response.data.traceId) ? error.response.data.traceId:'';
									let status_code = (error.response.status) ? error.response.status:'';
									let error_message = (error.response.data.errors) ? error.response.data.errors[0].errorMessage.en:'';
									if(error_message){
										reject('Status code: '+status_code+ '; TraceId: ' +trace_id+ '; Message: ' +error_message);
									}else if(error.response.data.error_description){
										reject('Message: ' +error.response.data.error_description);
									}else{
										reject('Login Fail');
									}
								}else if (error.request) {
									console.log(error.request);
									reject('Message: ' +error.message);
								}else {
									console.log(error);
									reject('Message: ' +error.message);
								}
								//reject('Login Fail');
						  });
						}else{
							//user['data']['logoURL'] = "";
							resolve(user);
							user = this.switchAdminaAndSuper(user,path);
						}
                    }
                    else
                    {
                        reject(response.data.error);
                    }
                }).catch(function (error) {
					if (error.response) {
						console.log(error.response);
						let message = {};
						message['traceId'] = (error.response.data.traceId) ? error.response.data.traceId:'';
						message['status'] = (error.response.status) ? error.response.status:'';
						message['message'] = (error.response.data.errors) ? error.response.data.errors[0].errorMessage.en:'';
						if(message['message']){
							console.log(message);
							reject(message);
						}else if(error.response.data.error_description){
							reject('Message: ' +error.response.data.error_description);
						}else{
							reject('Login Fail');
						}
					}else if (error.request) {
						console.log(error.request);
						reject('Message: ' +error.message);
					}else {
						console.log(error);
						reject('Message: ' +error.message);
					}
					//reject('Login Fail');
			  });
        });
    };

    setUserData = decodedJwtData => {

		
		const jwtGroups = decodedJwtData.groups;
		const kaching_admin = 'Internal/kaching_admin';
		const kaching_seller = 'Internal/kaching_seller';
		const kaching_store_manager = 'Internal/kaching_store_manager';
		const kaching_super_admin = 'kaching_super_admin';
		const kaching_partner_admin = 'kaching_partner_admin';
		let userrole = '';
		let logoURL = '';
		let user = {};
		if(jwtGroups.includes(kaching_seller)){
			userrole = 'Seller';
		}
		if(jwtGroups.includes(kaching_store_manager)){
			userrole = 'StoreManager';
		}
		if(jwtGroups.includes(kaching_admin)){
			userrole = 'admin';
		}
		if(jwtGroups.includes(kaching_super_admin)){
			userrole = 'superadmin';
			return user =
				{
				role    : userrole,
				data    : {
					'displayName': decodedJwtData.sub,
					'orgName': decodedJwtData.sub,
					'userId' : decodedJwtData['user-id'],
					'logoURL': logoURL
				}
			};
		}
		if(jwtGroups.includes(kaching_partner_admin)){
			userrole = 'partneradmin';
			return user =
				{
				role    : userrole,
				data    : {
					'displayName': decodedJwtData.sub,
					'orgName': decodedJwtData.sub,
					'userId' : decodedJwtData['user-id'],
					'logoURL': logoURL
				}
			};
		}
		return new Promise((resolve, reject) => {
			axios.get(ORGANIZATION_URL)
				.then(response => {
					if ( response.data )
					{
						let user = {};
						let data_to_post_qs = {"organizationId":response.data.response.id,"email":response.data.response.contactEmail}
						const qs_request = axios.post(DASHBOARD_URL+'/users/register', data_to_post_qs);
						if(userrole){
							user =
							{
								role    : userrole,
								data    : {
									'displayName': decodedJwtData.sub,
									'orgName': response.data.response.name,
									'userId' : decodedJwtData['user-id'],
									'logoURL': (response.data.response.logoUrl) ? response.data.response.logoUrl:''
								}
							};
						}else{
							this.setSession(null);
						}
						//if(userrole == 'admin'){
							user = this.checkForOnboard(user)
						//}
						resolve(user);
					}
					else
					{
						reject(response.data.error);
					}
				}).catch(function (error) {
					let message = {};
					if (error.response) {
						console.log(error.response);
						message['traceId'] = (error.response.data.traceId) ? error.response.data.traceId:'';
						message['status'] = (error.response.status) ? error.response.status:'';
						message['message'] = (error.response.data.errors) ? error.response.data.errors[0].errorMessage.en:'';
						if(message['message']){
							console.log(message);
							reject(message);
						}else if(error.response.data.error_description){
							reject('Message: ' +error.response.data.error_description);
						}else{
							reject('Login Fail');
						}
					}else if (error.request) {
						console.log(error.request);
						message['message'] = error.message;
						reject(message);
					}else {
						console.log(error);
						message['message'] = error.message;
						reject(message);
					}
					//reject('Login Fail');
			  });
		});
	}
    //~ setUserRole = username => {
        //~ return new Promise((resolve, reject) => {
            //~ axios.get('/api/users/2.0/user/'+username)
                //~ .then(response => {
                    //~ if ( response.data.userName )
                    //~ {
						//~ let userrole = '';
						//~ if(response.data.roleList){
							//~ if(response.data.roleList.MerchantAdmin){
								//~ userrole = 'admin';
							//~ }
							//~ if(response.data.roleList.Seller){
								//~ userrole = 'Seller';
							//~ }
							//~ if(response.data.roleList.StoreManager){
								//~ userrole = 'StoreManager';
							//~ }
						//~ }
						//~ let user = {};
						//~ if(userrole){
							//~ user =
							//~ {
								//~ role    : userrole,
								//~ data    : {
									//~ 'displayName': response.data.userName
								//~ }
							//~ };
						//~ }else{
							//~ this.setSession(null);
						//~ }
						
						//~ if(userrole == 'admin'){
							//~ user = this.checkForOnboard(user)
						//~ }
						//~ resolve(user);
                    //~ }
                    //~ else
                    //~ {
                        //~ reject(response.data.error);
                    //~ }
                //~ });
        //~ });
			
	//~ }

	checkForOnboard = user => {
        return new Promise((resolve, reject) => {
            axios.get(CATALOG_URL+'/info')
                .then(response => {
                    if ( response.data )
                    {
						if(!response.data.response.productCount || response.data.response.productCount <= 0){
							if(user.role == 'Seller' || user.role == 'StoreManager'){
								let message = {
									message : "Access denied. The onboarding process for the account is not complete.  Please contact your Merchant Administrator so they may complete the onboarding."
								};
								reject(message);
								this.setSession(null);
							}
							const onbordFinish = window.localStorage.getItem('onboard_finish');
							const orgId = window.localStorage.getItem('orgId');
							let userrole = 'onboard';
							if(!orgId && user.role=="superadmin"){
								userrole = 'superadmin';
							}else if(!orgId && user.role=="partneradmin"){
								userrole = 'partneradmin';
							}else if(onbordFinish){
								if(user.role=="superadmin"){
									userrole = "superadmin"
								}else if(user.role=="partneradmin"){
									userrole = "partneradmin"
								}else{
									userrole = 'admin';
								}
							}
							user =
							{
								role    : userrole,
								data    : {
									'displayName': user.data.displayName,
									'orgName': user.data.orgName,
									'userId': user.data.userId,
									'logoURL': user.data.logoURL ? user.data.logoURL : ""
								}
							};

							if(userrole == 'onboard' && window.location.pathname != "/onboard" && window.location.pathname != "/profile"){
								history.push("/");
							}
						}
						if(response.data.response.productCount && window.location.pathname == "/onboard"){
							history.push("/");
						}
						resolve(user);
                    }
                    else
                    {
                        reject(response.data.Message);
                    }
                }).catch(function (error) {
					let message = {};
					if (error.response) {
						console.log(error.response);
						message['traceId'] = (error.response.data.traceId) ? error.response.data.traceId:'';
						message['status'] = (error.response.status) ? error.response.status:'';
						message['message'] = (error.response.data.errors) ? error.response.data.errors[0].errorMessage.en:'';
						if(message['message']){
							console.log(message);
							reject(message);
						}else if(error.response.data.error_description){
							reject('Message: ' +error.response.data.error_description);
						}else{
							reject('Login Fail');
						}
					}else if (error.request) {
						console.log(error.request);
						message['message'] = error.message;
						reject(message);
					}else {
						console.log(error);
						message['message'] = error.message;
						reject(message);
					}
					//reject('Login Fail');
			  });
        });
	}

    //~ updateUserData = (user) => {
        //~ return axios.post('/api/auth/user/update', {
            //~ user: user
        //~ });
    //~ };

    switchAdminaAndSuper = (user,path) => {
		if(user && !user.role){
			user.then(response => {	
				user = response;
				if(user.role == 'superadmin' && path.includes('/superadmin') != true){
					user.role = 'admin';
					user.data.orgName = this.getOrganizationName();
					return user;
				}
				if(user.role == 'partneradmin' && path.includes('/partneradmin') != true){
					user.role = 'admin';
					user.data.orgName = this.getOrganizationName();
					return user;
				}
			});
		}
		if(path.includes('/superadmin') || path.includes('/partneradmin')){
			this.removeOrgDataSession();
		}
		return user;
	}

    removeOrgDataSession = () => {
		localStorage.removeItem('orgId');
		localStorage.removeItem('org_Name');
        localStorage.removeItem('onboard_finish');
		delete axios.defaults.headers.common['organizationId'];
		this.removeCookies();
	}
    getOrganizationName = () => {
		return window.localStorage.getItem('org_Name');
	}
    getOrganizationId = () => {
		return window.localStorage.getItem('orgId');
	}

    setSession = access_token => {
        if ( access_token )
        {
            localStorage.setItem('jwt_access_token', access_token);
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
            let orgId = this.getOrganizationId();
            if(orgId){
				axios.defaults.headers.common['organizationId'] = orgId;
			}
        }
        else
        {
            localStorage.removeItem('jwt_access_token');
            localStorage.removeItem('jwt_id_token');
			localStorage.removeItem('jwt_refresh_token');
			localStorage.removeItem('onboard_finish');
            delete axios.defaults.headers.common['Authorization'];
            this.removeOrgDataSession();
			this.removeCookies();
        }
    };

    logout = () => {
		let access_token = this.getAccessToken();
		const aheaders = this.getEncodedString();
		const data = "token="+access_token;
		delete axios.defaults.headers.common['organizationId'];
		axios.post('/revoke', data, {
                headers: aheaders
            }).then(response => {
				console.log(response.status);
			}).catch(function (error) {
				if (error.response) {
					console.log(error.response);
				}else if (error.request) {
					console.log(error.request);
				}else {
					console.log(error);
				}
		  });

        this.setSession(null);
    };

    isAuthTokenValid = access_token => {
        if ( !access_token )
        {
            return false;
        }
        if ( !this.getIdToken())
        {
            return false;
        }
        const decoded = jwtDecode(this.getIdToken());
        const currentTime = (Date.now() / 1000);
        if ( decoded.exp < currentTime )
        {
			return false;
		}
        else
        {
            return true;
        }
    };

    getAccessToken = () => {
        return window.localStorage.getItem('jwt_access_token');
	};

    getRefreshToken = () => {
        return window.localStorage.getItem('jwt_refresh_token');
	};

    getIdToken = () => {
        return window.localStorage.getItem('jwt_id_token');
	};

	refreshToken = () => {
		const refreshToken = this.getRefreshToken();
        const aheaders = this.getEncodedString();
        const data = "grant_type=refresh_token&refresh_token="+refreshToken;
		delete axios.defaults.headers.common['organizationId'];
		return new Promise((resolve, reject) => {
			axios.post('/token', data, {
                headers: aheaders
            }).then(response => {
                if (response && response.data.access_token)
                {
					this.setSession(response.data.access_token);
                    localStorage.setItem('jwt_id_token', response.data.id_token);
					localStorage.setItem('jwt_refresh_token', response.data.refresh_token);
					resolve(true);
                }
                else if (response && response.data.error_description )
                {
                    reject(response.data.error_description);
                }
            }).catch(error => {
				this.setSession(null);
				history.push("/");
				reject(error);
			  })
			});
    };

    getEncodedString = () => {

    	let CONSUMER_KEY = process.env.REACT_APP_CONSUMER_KEY;
		let CONSUMER_SECRET = process.env.REACT_APP_CONSUMER_SECRET;
		const encodedString = new Buffer(CONSUMER_KEY+':'+CONSUMER_SECRET).toString('base64');
		const aheaders = {
		  'Authorization': 'Basic '+encodedString,
		}
		return aheaders;
    };

    getDeviceScope = () =>{
    	let result='';
		let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		for (let i=0;i<8;i++) {
		  result += characters.charAt(Math.floor(Math.random() * characters.length));
		}
		result = "device_"+result;
		return result;
    }

    removeCookies = () => {
    	cookies.remove("productStoreDropdown", { path: '/' });
        cookies.remove("productWarehouseDropdown", { path: '/' });
    }
}

const instance = new jwtService();

export default instance;
