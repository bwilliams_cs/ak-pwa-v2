import React from 'react';
import axios from 'axios';
import jwtService from '../jwtService';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

describe('jwtService', () =>
    it('sets the value of an last_access_time to localstorage', () => {
        jwtService.setLastAccessTime()
        expect(localStorage.getItem('last_access_time')).toEqual(JSON.stringify(Date.now() / 1000));
    }),
    it('should mock hasUserIdleTimeExceeded to be true', () => {
        expect(jwtService.hasUserIdleTimeExceeded()).toBe(true);
    }),
    it('should mock hasUserIdleTimeExceeded to be false', () => {
        jwtService.setLastAccessTime()
        expect(jwtService.hasUserIdleTimeExceeded()).toBe(false);
    }),
    it('should mock isAuthTokenValid to be false', () => {
        expect(jwtService.isAuthTokenValid()).toBe(false);
    }),
    it('should mock isAuthTokenValid to be true', () => {
        window.localStorage.setItem('jwt_id_token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImp0aSI6ImRhMDUyZmIxLWQ5NmEtNDcxZS1iM2FkLWU2NDMwZWI1OWY3NiIsImlhdCI6MTU4OTUyMzM4NywiZXhwIjoxNTg5NTI3MDg1fQ.iVwUslt8RRgXAoI4APDCuvq4PKNosekWm6NR2EGl_LY');
        expect(jwtService.isAuthTokenValid("722d4828-05ca-308d-bcff-1e30acd677d3")).toBe(true);
    }),
    it('should mock setSession to null', () => {
        jwtService.setSession(null);
        expect(localStorage.store).toBeUndefined();
    }),
    it('should mock setSession with access token', () => {
        let access_token = jwtService.getAccessToken();
        jwtService.setSession(access_token);
        expect(localStorage.getItem('jwt_access_token')).toEqual(access_token);
    }),
);
